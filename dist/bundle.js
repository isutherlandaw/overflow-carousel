/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = overflowCarousel;

var _OverflowCarousel = __webpack_require__(2);

var _OverflowCarousel2 = _interopRequireDefault(_OverflowCarousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * Create an overflow carousel.
 *
 * Example Markup:
 *
 *    <div class="overflow-carousel">
 *        <div class="overflow-carousel__container">
 *            <ul class="overflow-carousel__content">
 *                <li class="overflow-carousel__item">
 *                    {Your content}
 *                </li>
 *            </ul>
 *        </div>
 *    </div>
 *
 * Usage: overflowCarousel(DOMElement, [options])
 *
 * Options:
 * {
 * 	selector : 'overflow-carousel',		// The class name of the carousel element
 * 	speed: 500,							// The animation speed
 * 	hideScrollbar: true,				// Try and hide the scrollbars
 * 	itemWidth: 500						// Force the width of each carousel item to a set width
 * 	forceNavigation: false;				// Always show the navigation, default detects a mouse pointer
 * }
 *
 * @param DOMElement
 * @param selector
 * @param speed
 * @param hideScrollbar
 * @param itemWidth
 * @param forceNavigation
 * @param debug
 * @returns {OverflowCarousel}
 */
function overflowCarousel(DOMElement) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$selector = _ref.selector,
      selector = _ref$selector === undefined ? 'overflow-carousel' : _ref$selector,
      _ref$speed = _ref.speed,
      speed = _ref$speed === undefined ? 1000 : _ref$speed,
      _ref$hideScrollbar = _ref.hideScrollbar,
      hideScrollbar = _ref$hideScrollbar === undefined ? true : _ref$hideScrollbar,
      _ref$itemWidth = _ref.itemWidth,
      itemWidth = _ref$itemWidth === undefined ? 0 : _ref$itemWidth,
      _ref$forceNavigation = _ref.forceNavigation,
      forceNavigation = _ref$forceNavigation === undefined ? false : _ref$forceNavigation,
      _ref$debug = _ref.debug,
      debug = _ref$debug === undefined ? true : _ref$debug;

  try {

    var content = DOMElement.querySelector('.' + selector + '__content');
    var container = DOMElement.querySelector('.' + selector + '__container');
    var items = [].concat(_toConsumableArray(DOMElement.querySelectorAll('.' + selector + '__item')));

    var options = {
      speed: speed,
      name: selector,
      itemWidth: itemWidth,
      forceNavigation: forceNavigation,
      hideScrollbar: hideScrollbar
    };

    return new _OverflowCarousel2.default(DOMElement, content, container, items, options);
  } catch (error) {
    // Allow a debug option
    if (debug) {
      console.log(error);
    }
  }
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _overflowCarousel = __webpack_require__(0);

var _overflowCarousel2 = _interopRequireDefault(_overflowCarousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

[].concat(_toConsumableArray(document.querySelectorAll('.overflow-carousel'))).forEach(function (carousel) {
	(0, _overflowCarousel2.default)(carousel, { speed: 750 });
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var OverflowCarousel = function () {

	/**
  * Set up our Carousel
  * @param carousel
  * @param content
  * @param container
  * @param items
  * @param options
  */
	function OverflowCarousel(carousel, content, container, items, options) {
		var _this = this;

		_classCallCheck(this, OverflowCarousel);

		this.carousel = carousel;
		this.content = content;
		this.container = container;
		this.items = items;
		this.options = options;
		this.scrollPad = 30;
		this.btnLeft = null;
		this.btnRight = null;
		this.nav = null;

		// Apply any user dimensions, then calculate the length
		this.applyUserDimensions(options.itemWidth);
		this.height = this.calculateContentHeight();

		// Account for the 'View more' link, then fix the dimensions
		this.positionMoreLink();
		this.width = this.calculateContentWidth();
		this.fixCarouselDimensions();

		// Hide the scroll bars
		if (this.options.hideScrollbar === true) {
			this.hideScrollBar();
		}

		// Handle navigation controls, based on a mouse being used.
		if (this.options.forceNavigation) {
			this.createControls();
		} else {
			this.carousel.addEventListener('mouseenter', function () {
				_this.createControls();
			}, true);
		}
	}

	/**
  * Calculate the total width of the elements in the carousel
  *
  * @returns {*}
  */


	_createClass(OverflowCarousel, [{
		key: 'calculateContentWidth',
		value: function calculateContentWidth() {
			return this.items.reduce(function (width, item) {
				return width + item.getBoundingClientRect().width;
			}, 0);
		}

		/**
   * Calculate the tallest element in the carousel
   *
   * @returns {number}
   */

	}, {
		key: 'calculateContentHeight',
		value: function calculateContentHeight() {
			var height = 0;
			return Math.ceil(this.items.reduce(function (height, item) {
				var elementHeight = item.getBoundingClientRect().height;
				return elementHeight > height ? elementHeight : height;
			}, height));
		}

		/**
   * Set the correct width and height of the carousel based on the contents
   */

	}, {
		key: 'fixCarouselDimensions',
		value: function fixCarouselDimensions() {
			this.content.style.width = this.width + 'px';
			this.container.style.height = this.height + 'px';
			this.carousel.style.height = this.height + 'px';
		}

		/**
   * Apply a user forced width on the carousel items
   *
   * @param itemWidth
   */

	}, {
		key: 'applyUserDimensions',
		value: function applyUserDimensions(itemWidth) {
			var forceWidth = Number(itemWidth);

			if (forceWidth && forceWidth > 0) {
				this.items.forEach(function (item) {
					item.style.width = forceWidth + 'px';
					item.style.maxWidth = forceWidth + 'px';
				});
			}
		}

		/**
   * Hide the scrollbars for the overflow content
   */

	}, {
		key: 'hideScrollBar',
		value: function hideScrollBar() {
			this.container.style.height = this.height + this.scrollPad + 'px';
		}

		/**
   * Based on the given direction, work out if the element is visible
   *
   * @param element
   * @param direction
   * @returns {boolean}
   */

	}, {
		key: 'isVisible',
		value: function isVisible(element, direction) {
			var eb = element.getBoundingClientRect();
			var pb = this.carousel.getBoundingClientRect();

			return direction === 'left' ? eb.left - pb.left < 0 : eb.left - pb.left + eb.width > pb.width;
		}

		/**
   * Based on the given direction, get the first hidden, or partially hidden element
   *
   * @param direction
   * @returns {T}
   */

	}, {
		key: 'getTargetElement',
		value: function getTargetElement(direction) {
			var _this2 = this;

			var visible = this.items.filter(function (item) {
				return _this2.isVisible(item, direction);
			});

			return direction === 'left' ? visible.pop() : visible[0];
		}

		/**
   * Based on the given direction, Calculate the distance to scroll
   *
   * @param target
   * @param direction
   * @returns {number}
   */

	}, {
		key: 'calculateDistance',
		value: function calculateDistance(target, direction) {
			if (!target) {
				return false;
			}
			var eb = target.getBoundingClientRect();
			var pb = this.carousel.getBoundingClientRect();

			return direction === 'left' ? pb.left + pb.width - (eb.left + eb.width) : eb.left - pb.left;
		}

		/**
   * Set button state by adding or removing the state class
   * @param btn
   * @param state
   * @param predicate
   */

	}, {
		key: 'setButtonState',
		value: function setButtonState(btn, state, predicate) {
			predicate ? btn.classList.remove(state) : btn.classList.add(state);
		}

		/**
   * Show and hide the navigation buttons appropriately
   */

	}, {
		key: 'controlButtonDisplay',
		value: function controlButtonDisplay() {
			this.setButtonState(this.btnLeft, 'visible', this.container.scrollLeft === 0);
			this.setButtonState(this.btnRight, 'visible', this.container.scrollLeft + this.container.offsetWidth >= this.container.scrollWidth);
		}

		/**
   * 	Create a navigation button for the given direction
   *
   * @param direction
   * @returns {Element}
   */

	}, {
		key: 'createButton',
		value: function createButton(direction) {
			var _this3 = this;

			var btn = document.createElement('span');
			btn.classList.add(this.options.name + '__nav-btn');
			btn.setAttribute('data-direction', direction);
			btn.addEventListener('click', function () {
				_this3.handleScroll(direction);
			}, false);

			return btn;
		}

		/**
   * Create the button navigation for the carousel
   *
   * @returns {Element}
   */

	}, {
		key: 'createNavigation',
		value: function createNavigation() {
			this.nav = document.createElement('div');
			this.nav.classList.add(this.options.name + '__nav');
			this.nav.appendChild(this.btnLeft);
			this.nav.appendChild(this.btnRight);

			return this.nav;
		}

		/**
   * Create the navigation controls
   * We want to hide them for touch devices, unless forced to show by user options
   * @returns {boolean}
   */

	}, {
		key: 'createControls',
		value: function createControls() {
			var _this4 = this;

			if (this.nav !== null) {
				return false;
			}

			this.btnLeft = this.createButton('left');
			this.btnRight = this.createButton('right');
			this.carousel.appendChild(this.createNavigation());

			// Handle the button display
			this.container.addEventListener('scroll', function () {
				_this4.controlButtonDisplay();
			});
			window.addEventListener('resize', function () {
				_this4.controlButtonDisplay();
			});
			this.controlButtonDisplay();
			this.hasControls = true;
		}

		/**
   * Pixels per millisecond
   * Even out the disparity in scroll speed over long vs short distance at a constant speed
   *
   * @param duration
   * @param distance
   * @returns {number}
   */

	}, {
		key: 'calculateSpeed',
		value: function calculateSpeed(duration, distance) {
			return distance * (duration / 1000);
		}

		/**
   * Handle the scrolling animation
   *
   * @param scroll
   * @param duration
   */

	}, {
		key: 'animate',
		value: function animate(scroll, duration) {

			var start = performance.now();

			requestAnimationFrame(function animate(time) {

				var fraction = (time - start) / duration;
				if (fraction > 1) {
					fraction = 1;
				}

				scroll(fraction);

				if (fraction < 1) {
					requestAnimationFrame(animate);
				}
			});
		}

		/**
   * Handles a click event on a navigation button
   *
   * @param direction
   */

	}, {
		key: 'handleScroll',
		value: function handleScroll(direction) {
			var _this5 = this;

			var start = this.container.scrollLeft;
			var target = this.getTargetElement(direction);
			var distance = this.calculateDistance(target, direction);
			var speed = this.calculateSpeed(this.options.speed, distance);

			if (target && direction) {
				this.animate(function (progress) {
					direction === 'left' ? _this5.container.scrollLeft = start - progress * distance : _this5.container.scrollLeft = start + progress * distance;
				}, speed);
			}
		}

		/**
   * See if we have a 'View more' link
   *
   * @param element
   * @returns {Element}
   */

	}, {
		key: 'findMoreLink',
		value: function findMoreLink(element) {
			return element.querySelector('.' + this.options.name + '__more');
		}

		/**
   * Position the 'View More' link, if we have one
   */

	}, {
		key: 'positionMoreLink',
		value: function positionMoreLink() {
			var _this6 = this;

			var more = this.items.filter(function (item) {
				return !!_this6.findMoreLink(item);
			});

			if (more.length > 0) {
				more = more[0];
				more.setAttribute('style', 'padding-right: 0; transform: translateY(-50%); margin-top: ' + this.height / 2 + 'px');
			}
		}
	}]);

	return OverflowCarousel;
}();

exports.default = OverflowCarousel;

/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map