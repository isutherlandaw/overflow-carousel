export default class OverflowCarousel {

	/**
	 * Set up our Carousel
	 * @param carousel
	 * @param content
	 * @param container
	 * @param items
	 * @param options
	 */
	constructor(carousel,
				content,
				container,
				items,
				options) {
		this.carousel = carousel
		this.content = content
		this.container = container
		this.items = items
		this.options = options
		this.scrollPad = 30
		this.btnLeft = null
		this.btnRight  = null
		this.nav  = null

		// Apply any user dimensions, then calculate the length
		this.applyUserDimensions(options.itemWidth)
		this.height = this.calculateContentHeight()

		// Account for the 'View more' link, then fix the dimensions
		this.positionMoreLink()
		this.width = this.calculateContentWidth()
		this.fixCarouselDimensions()

		// Hide the scroll bars
		if (this.options.hideScrollbar === true) {
			this.hideScrollBar()
		}

		// Handle navigation controls, based on a mouse being used.
		if (this.options.forceNavigation) {
			this.createControls()
		} else {
			this.carousel.addEventListener('mouseenter', () => {
				this.createControls()
			}, true)
		}
	}

	/**
	 * Calculate the total width of the elements in the carousel
	 *
	 * @returns {*}
	 */
	calculateContentWidth() {
		return this.items.reduce((width, item) => {
			return width + item.getBoundingClientRect().width
		}, 0)
	}

	/**
	 * Calculate the tallest element in the carousel
	 *
	 * @returns {number}
	 */
	calculateContentHeight() {
		let height = 0
		return Math.ceil(this.items.reduce((height, item) => {
			const elementHeight = item.getBoundingClientRect().height
			return (elementHeight > height) ? elementHeight : height
		}, height))
	}

	/**
	 * Set the correct width and height of the carousel based on the contents
	 */
	fixCarouselDimensions() {
		this.content.style.width = `${this.width}px`
		this.container.style.height = `${this.height}px`
		this.carousel.style.height = `${this.height}px`
	}

	/**
	 * Apply a user forced width on the carousel items
	 *
	 * @param itemWidth
	 */
	applyUserDimensions(itemWidth) {
		let forceWidth = Number(itemWidth)

		if (forceWidth && forceWidth > 0) {
			this.items.forEach((item) => {
				item.style.width = `${forceWidth}px`
				item.style.maxWidth = `${forceWidth}px`
			})
		}
	}

	/**
	 * Hide the scrollbars for the overflow content
	 */
	hideScrollBar() {
		this.container.style.height = `${this.height + this.scrollPad}px`
	}

	/**
	 * Based on the given direction, work out if the element is visible
	 *
	 * @param element
	 * @param direction
	 * @returns {boolean}
	 */
	isVisible(element, direction) {
		const eb = element.getBoundingClientRect()
		const pb = this.carousel.getBoundingClientRect()

		return (direction === 'left') ? eb.left - pb.left < 0 : ((eb.left - pb.left) + eb.width) > pb.width
	}

	/**
	 * Based on the given direction, get the first hidden, or partially hidden element
	 *
	 * @param direction
	 * @returns {T}
	 */
	getTargetElement(direction) {
		const visible = this.items.filter((item) => this.isVisible(item, direction))

		return (direction === 'left') ? visible.pop() : visible[0]
	}

	/**
	 * Based on the given direction, Calculate the distance to scroll
	 *
	 * @param target
	 * @param direction
	 * @returns {number}
	 */
	calculateDistance(target, direction) {
		if(!target){
			return false
		}
		const eb = target.getBoundingClientRect()
		const pb = this.carousel.getBoundingClientRect()

		return (direction === 'left') ? (pb.left + pb.width) - (eb.left + eb.width) : eb.left - pb.left
	}

	/**
	 * Set button state by adding or removing the state class
	 * @param btn
	 * @param state
	 * @param predicate
	 */
	setButtonState(btn, state, predicate) {
		(predicate) ? btn.classList.remove(state) : btn.classList.add(state)
	}

	/**
	 * Show and hide the navigation buttons appropriately
	 */
	controlButtonDisplay() {
		this.setButtonState(this.btnLeft, 'visible', (this.container.scrollLeft === 0))
		this.setButtonState(this.btnRight, 'visible', (this.container.scrollLeft + this.container.offsetWidth) >= this.container.scrollWidth)
	}

	/**
	 * 	Create a navigation button for the given direction
	 *
	 * @param direction
	 * @returns {Element}
	 */
	createButton(direction) {
		const btn = document.createElement('span')
		btn.classList.add(this.options.name + '__nav-btn')
		btn.setAttribute('data-direction', direction)
		btn.addEventListener('click', () => {
			this.handleScroll(direction)
		}, false)

		return btn
	}

	/**
	 * Create the button navigation for the carousel
	 *
	 * @returns {Element}
	 */
	createNavigation() {
		this.nav = document.createElement('div')
		this.nav.classList.add(this.options.name + '__nav')
		this.nav.appendChild(this.btnLeft)
		this.nav.appendChild(this.btnRight)

		return this.nav
	}

	/**
	 * Create the navigation controls
	 * We want to hide them for touch devices, unless forced to show by user options
	 * @returns {boolean}
	 */
	createControls() {

		if (this.nav !== null) {
			return false
		}

		this.btnLeft = this.createButton('left')
		this.btnRight = this.createButton('right')
		this.carousel.appendChild(this.createNavigation())

		// Handle the button display
		this.container.addEventListener('scroll', () => {
			this.controlButtonDisplay()
		})
		window.addEventListener('resize', () => {
			this.controlButtonDisplay()
		})
		this.controlButtonDisplay()
		this.hasControls = true
	}

	/**
	 * Pixels per millisecond
	 * Even out the disparity in scroll speed over long vs short distance at a constant speed
	 *
	 * @param duration
	 * @param distance
	 * @returns {number}
	 */
	calculateSpeed(duration, distance) {
		return distance * (duration / 1000)
	}

	/**
	 * Handle the scrolling animation
	 *
	 * @param scroll
	 * @param duration
	 */
	animate(scroll, duration) {

		let start = performance.now()

		requestAnimationFrame(function animate(time) {

			let fraction = (time - start) / duration
			if (fraction > 1) {
				fraction = 1
			}

			scroll(fraction)

			if (fraction < 1) {
				requestAnimationFrame(animate)
			}
		})
	}

	/**
	 * Handles a click event on a navigation button
	 *
	 * @param direction
	 */
	handleScroll(direction) {
		const start = this.container.scrollLeft
		const target = this.getTargetElement(direction)
		const distance = this.calculateDistance(target, direction)
		const speed = this.calculateSpeed(this.options.speed, distance)

		if (target && direction) {
			this.animate((progress) => {
				(direction === 'left') ?
						this.container.scrollLeft = start - (progress * distance) :
						this.container.scrollLeft = start + (progress * distance)
			}, speed)
		}
	}

	/**
	 * See if we have a 'View more' link
	 *
	 * @param element
	 * @returns {Element}
	 */
	findMoreLink(element) {
		return element.querySelector('.' + this.options.name + '__more')
	}

	/**
	 * Position the 'View More' link, if we have one
	 */
	positionMoreLink() {
		let more = this.items.filter((item) => !!this.findMoreLink(item))

		if (more.length > 0) {
			more = more[0]
			more.setAttribute(
					'style',
					`padding-right: 0; transform: translateY(-50%); margin-top: ${this.height / 2}px`)
		}
	}
}