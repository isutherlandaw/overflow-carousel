import OverflowCarousel from './OverflowCarousel';

/**
 * Create an overflow carousel.
 *
 * Example Markup:
 *
 *    <div class="overflow-carousel">
 *        <div class="overflow-carousel__container">
 *            <ul class="overflow-carousel__content">
 *                <li class="overflow-carousel__item">
 *                    {Your content}
 *                </li>
 *            </ul>
 *        </div>
 *    </div>
 *
 * Usage: overflowCarousel(DOMElement, [options])
 *
 * Options:
 * {
 * 	selector : 'overflow-carousel',		// The class name of the carousel element
 * 	speed: 500,							// The animation speed
 * 	hideScrollbar: true,				// Try and hide the scrollbars
 * 	itemWidth: 500						// Force the width of each carousel item to a set width
 * 	forceNavigation: false;				// Always show the navigation, default detects a mouse pointer
 * }
 *
 * @param DOMElement
 * @param selector
 * @param speed
 * @param hideScrollbar
 * @param itemWidth
 * @param forceNavigation
 * @param debug
 * @returns {OverflowCarousel}
 */
export default function overflowCarousel(DOMElement,
										 {
											 selector: selector = 'overflow-carousel',
											 speed: speed = 1000,
											 hideScrollbar: hideScrollbar = true,
											 itemWidth: itemWidth = 0,
											 forceNavigation: forceNavigation = false,
											 debug = true
										 } = {}) {

	try {

		const content = DOMElement.querySelector('.' + selector + '__content');
		const container = DOMElement.querySelector('.' + selector + '__container');
		const items = [...DOMElement.querySelectorAll('.' + selector + '__item')];

		const options = {
			speed: speed,
			name: selector,
			itemWidth: itemWidth,
			forceNavigation: forceNavigation,
			hideScrollbar: hideScrollbar
		}

		return new OverflowCarousel(DOMElement, content, container, items, options);

	} catch (error) {
		// Allow a debug option
		if(debug) {
			console.log(error);
		}
	}
}