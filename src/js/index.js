import overflowCarousel from '../lib/overflow-carousel';

[...document.querySelectorAll('.overflow-carousel')].forEach((carousel) => {
	overflowCarousel(carousel, {speed: 750});
});